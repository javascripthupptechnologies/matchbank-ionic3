import { Component, OnInit } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { FormControl, FormGroup, Validators,ValidatorFn,AbstractControl } from '@angular/forms';
import { TabsPage } from '../../tabs/tabs'

@Component({
  selector: 'page-account-detials',
  templateUrl: 'account-detials.html',
})
export class AccountDetialsPage implements OnInit  {
  user: FormGroup;
	public account_detials:any;
  public active:boolean =  false;
  public show_hide_feild:boolean =  false;
  public save:boolean =  false;
  public main_contain:boolean =  false;
  status: boolean = false;
  status1: boolean = false;


  ngOnInit() {

    this.user = new FormGroup({
    cpassword: new FormControl('', [Validators.required, Validators.minLength(6),Validators.maxLength(10)]),
    addpassword: new FormControl('', [Validators.required]),
    re_password: new FormControl('', [Validators.required,this.equalto('addpassword')])
    });



    }

    equalto(field_name): ValidatorFn {
      return (control: AbstractControl): {[key: string]: any} => {

      let input = control.value;

      let isValid=control.root.value[field_name]==input
      if(!isValid) 
      return { 'equalTo': {isValid} }
      else 
      return null;
      };
      }
  constructor(public navCtrl: NavController, public navParams: NavParams) {
  	this.account_detials = "Step1";
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AccountDetialsPage');
  }


  ngAfterViewInit() {
    let tabs = document.querySelectorAll('.show-tabbar');
    if (tabs !== null) {
        Object.keys(tabs).map((key) => {
            tabs[key].style.display = 'none';
        });
    }
}
 ionViewWillLeave() {
        let tabs = document.querySelectorAll('.show-tabbar');
        if (tabs !== null) {
            Object.keys(tabs).map((key) => {
                tabs[key].style.display = 'flex';
            });

        }
}

  onclickButton(actives){
    console.log("click",actives);
    this.active = true;
    this.show_hide_feild = true;
    this.save = true;
    this.main_contain = true;

  }

  myReport(){
      this.navCtrl.push(TabsPage);
  } 
  clickEvent(){
      this.status = !this.status;       
  }
  clickPassword(){
      this.status1 = !this.status1;       
  }	
}

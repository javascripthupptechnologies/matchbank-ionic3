import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';

import { MyAccountPage } from './my-account';
import { AccountDetialsPage } from '../my-account/account-detials/account-detials';

@NgModule({
  declarations: [
    MyAccountPage,
    AccountDetialsPage
  ],
  imports: [
    IonicPageModule.forChild(MyAccountPage),
  ],
  entryComponents:[
  	AccountDetialsPage
  ]
})
export class MyAccountPageModule {}

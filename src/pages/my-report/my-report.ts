import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { CreditScorePage } from '../my-report/credit-score/credit-score';
import { LoansPage } from '../my-report/loans/loans';
import { CreditCardPage } from '../my-report/credit-card/credit-card';
import { OthersPage } from '../my-report/others/others';
import { AccountDetialsPage } from '../my-account/account-detials/account-detials';

@IonicPage()
@Component({
  selector: 'page-my-report',
  templateUrl: 'my-report.html',
})
export class MyReportPage {

	report_tab1 = CreditScorePage;
	report_tab2 = LoansPage;
	report_tab3 = CreditCardPage;
	report_tab4 = OthersPage;
		
	constructor(public navCtrl: NavController, public navParams: NavParams) {
	  	
	}

	ionViewDidLoad() {
	    console.log('ionViewDidLoad MyReportPage');
	}

	goToaccount(){
	    this.navCtrl.setRoot(AccountDetialsPage);
	} 

}

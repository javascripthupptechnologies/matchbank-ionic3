import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MyReportPage } from './my-report';

import { CreditScorePage } from '../my-report/credit-score/credit-score';
import { LoansPage } from '../my-report/loans/loans';
import { CreditCardPage } from '../my-report/credit-card/credit-card';
import { OthersPage } from '../my-report/others/others';

@NgModule({
  declarations: [
    MyReportPage,
    CreditScorePage,
    LoansPage,
    CreditCardPage,
    OthersPage
  ],
  imports: [
    IonicPageModule.forChild(MyReportPage)
  ],
  entryComponents:[
  	CreditScorePage,
  	LoansPage,
    CreditCardPage,
    OthersPage
  ]
})
export class MyReportPageModule {}

import { Component } from '@angular/core';
import {  NavController, NavParams } from 'ionic-angular';



@Component({
  selector: 'page-credit-score',
  templateUrl: 'credit-score.html',
})
export class CreditScorePage {
	
	public displayData:any;

	public selectedcolor:any;
	constructor(public navCtrl: NavController, public navParams: NavParams) {

		
		this.displayData = [
	      {
	        "color": "#6d9fdb",
	        "value": 1,
	        "check" : "true"
	      },
	      {
	        "color": "#5788c5",
	        "value": 2,
	        "check" : "false"
	      },
	      {
	        "color": "#4874af",
	        "value": 3,
	        "check" : "false"
	      },
	      {
	        "color": "#2b5c91",
	        "value": 4,
	        "check" : "false"
	      },
	      {
	        "color": "#1d4576",
	        "value": 5,
	        "check" : "false"
	      },
	      {
	        "color": "#64b8ee",
	        "value": 6,
	        "check" : "false"
	      },
	      {
	        "color": "#459cd5",
	        "value": 7,
	        "check" : "false"
	      },
	      {
	        "color": "#9561dd",
	        "value": 8,
	        "check" : "false"
	      },
	      {
	        "color": "#874fd1",
	        "value": 9,
	        "check" : "false"
	      },
	      {
	        "color": "#6c38ad",
	        "value": 10,
	        "check" : "false"
	      },
	    ];

	    this.selectedcolor = this.displayData[0];
	}

	setcolor(data){
		this.selectedcolor = data;
	}

	ionViewDidLoad() {
	    console.log('ionViewDidLoad CreditScorePage');
	}

}

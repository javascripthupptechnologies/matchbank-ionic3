import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { FormControl, FormGroup, Validators,ValidatorFn,AbstractControl } from '@angular/forms';
import { NewLoanPage } from '../../match/new-loan/new-loan';

@Component({
  selector: 'page-income',
  templateUrl: 'income.html',
})
export class IncomePage implements OnInit{

  public setting:any;
  user: FormGroup;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ngOnInit() {

  this.user = new FormGroup({
    gross: new FormControl('', [Validators.required, Validators.minLength(1),Validators.maxLength(10)]),
    nett: new FormControl('', [Validators.required, Validators.minLength(1),Validators.maxLength(10)]),
    rental: new FormControl('', [Validators.required, Validators.minLength(1),Validators.maxLength(10)]),
    last: new FormControl('', [Validators.required, Validators.minLength(1),Validators.maxLength(10)])
  });

    this.user.valueChanges.subscribe((data)=>{
      console.log("Called cads")
      console.log(data);
    })

  }


  ionViewDidLoad() {
    console.log('ionViewDidLoad IncomePage');
  }

  goTonewloan(){
	    // this.navCtrl.push(NewLoanPage);
      this.navCtrl.parent.select(2);
	} 

}

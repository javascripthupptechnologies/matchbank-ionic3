import { Component  } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ResultsPage } from '../../match/results/results';


@Component({
  selector: 'page-match-intro',
  templateUrl: 'match-intro.html',
})
export class MatchIntroPage {
  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MatchIntroPage');
  }

  	goToresult(){
      // this.tabRef.select(1);  
      // this.tabRef.select(2);
	    // this.navCtrl.push(ResultsPage);
      this.navCtrl.parent.select(3);

	} 
}

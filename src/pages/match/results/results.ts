import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';


@Component({
  selector: 'page-results',
  templateUrl: 'results.html',
})
export class ResultsPage {

	current: number = 27;
	  max: number = 100;
	  stroke: number = 15;
	  radius: number = 125;
	  semicircle: boolean = false;
	  rounded: boolean = false;
	  responsive: boolean = true;
	  clockwise: boolean = true;
	  color: string = '#26c942';
	  background: string = '#4f2980';
	  duration: number = 800;
	  animation: string = 'easeOutCubic';
	  animationDelay: number = 0;
	  animations: string[] = [];
	  gradient: boolean = false;
	  realCurrent: number = 0;
	  rate:number;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ResultsPage');
  }

}

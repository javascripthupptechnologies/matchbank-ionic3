import { NgModule } from '@angular/core';
import {RoundProgressModule} from 'angular-svg-round-progressbar';
import { IonicPageModule } from 'ionic-angular';
import { MatchPage } from './match';

import { MatchIntroPage } from '../match/match-intro/match-intro';
import { IncomePage } from '../match/income/income';
import { NewLoanPage } from '../match/new-loan/new-loan';
import { ResultsPage } from '../match/results/results';
import { NumberFormatterDirective } from "../../directives/number-formatter/number-formatter";
import { MyCurrencyPipe } from "../../directives/number-formatter/my-currency.pipe";
@NgModule({
  declarations: [
    MatchPage,
    MatchIntroPage,
    IncomePage,
    NewLoanPage,
    ResultsPage,
    NumberFormatterDirective,
    MyCurrencyPipe
  ],
  imports: [
    IonicPageModule.forChild(MatchPage),
    RoundProgressModule

  ],
  entryComponents:[
  	MatchIntroPage,
  	IncomePage,
    NewLoanPage,
    ResultsPage
  ],
  providers:[MyCurrencyPipe]
})
export class MatchPageModule {}

import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { MatchIntroPage } from '../match/match-intro/match-intro';
import { IncomePage } from '../match/income/income';
import { NewLoanPage } from '../match/new-loan/new-loan';
import { ResultsPage } from '../match/results/results';
import { AccountDetialsPage } from '../my-account/account-detials/account-detials';

@IonicPage()
@Component({
  selector: 'page-match',
  templateUrl: 'match.html',
})
export class MatchPage {

	match_tab1 = MatchIntroPage;
	match_tab2 = IncomePage;
	match_tab3 = NewLoanPage;
	match_tab4 = ResultsPage;	

	constructor(public navCtrl: NavController, public navParams: NavParams) {
	}

	ionViewDidLoad() {
	    console.log('ionViewDidLoad MatchPage');
	}

	goToaccount(){
	    this.navCtrl.setRoot(AccountDetialsPage);
	} 

}

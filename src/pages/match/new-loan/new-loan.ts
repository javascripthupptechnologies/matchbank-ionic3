import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { FormControl, FormGroup, Validators,ValidatorFn,AbstractControl } from '@angular/forms';
import { ResultsPage } from '../../match/results/results';


@Component({
  selector: 'page-new-loan',
  templateUrl: 'new-loan.html',
})
export class NewLoanPage implements OnInit {
  user: FormGroup;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ngOnInit() {

  this.user = new FormGroup({
  loan: new FormControl('', [Validators.required, Validators.minLength(1),Validators.maxLength(10)]),
  loantenure: new FormControl('', [Validators.required, Validators.minLength(1),Validators.maxLength(10)])
  });

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad NewLoanPage');
  }
  goToresult(){

	    this.navCtrl.parent.select(3);
	} 

}

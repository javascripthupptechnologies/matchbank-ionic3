import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ChangeLogPage } from '../change-log/change-log';

/**
 * Generated class for the AnnounceArticlePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-announce-article',
  templateUrl: 'announce-article.html',
})
export class AnnounceArticlePage {
	tabBarElement: any;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  	this.tabBarElement = document.querySelector('.announce_tab .tabbar.show-tabbar');
  	

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AnnounceArticlePage');
  }

  ionViewWillEnter() {
    this.tabBarElement.style.display = 'none';
  }
 
  ionViewWillLeave() {
    this.tabBarElement.style.display = 'flex';
  }


  goToback(){
        this.navCtrl.push(ChangeLogPage);
    } 


}

import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { AnnounceArticlePage } from '../announce-article/announce-article';



/**
 * Generated class for the ChangeLogPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-change-log',
  templateUrl: 'change-log.html',
})
export class ChangeLogPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ChangeLogPage');
  }

  goToarticle(){
      this.navCtrl.push(AnnounceArticlePage);
  } 
  
}

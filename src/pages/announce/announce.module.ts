import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AnnouncePage } from './announce';

import { AnnouncementsPage } from '../announce/announcements/announcements';
import { ChangeLogPage } from '../announce/change-log/change-log';
import { EventsPage } from '../announce/events/events';
import { AnnounceArticlePage } from '../announce/announce-article/announce-article';

@NgModule({
  declarations: [
    AnnouncePage,
    AnnouncementsPage,
    ChangeLogPage,
    EventsPage,
    AnnounceArticlePage


  ],
  imports: [
    IonicPageModule.forChild(AnnouncePage),
  ],
  entryComponents:[
  	AnnouncementsPage,
  	ChangeLogPage,
    EventsPage,
    AnnounceArticlePage
  ]
})

export class AnnouncePageModule {}

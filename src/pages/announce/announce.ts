import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { AnnouncementsPage } from '../announce/announcements/announcements';
import { ChangeLogPage } from '../announce/change-log/change-log';
import { EventsPage } from '../announce/events/events';
import { AccountDetialsPage } from '../my-account/account-detials/account-detials';

/**
 * Generated class for the AnnouncePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-announce',
  templateUrl: 'announce.html',
})
export class AnnouncePage {

	announce_tab1 = AnnouncementsPage;
	announce_tab2 = ChangeLogPage;
	announce_tab3 = EventsPage;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AnnouncePage');
  }

  goToaccount(){
      this.navCtrl.setRoot(AccountDetialsPage);
  } 

}

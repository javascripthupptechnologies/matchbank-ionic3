import { Component } from '@angular/core';

import { MyReportPage } from '../my-report/my-report';
import { MatchPage } from '../match/match';
import { AnnouncePage } from '../announce/announce';

@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {

  tab1Root = MyReportPage;
  tab2Root = MatchPage;
  tab3Root = AnnouncePage;
  constructor() {

  }
}

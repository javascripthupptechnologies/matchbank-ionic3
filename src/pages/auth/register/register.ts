import { Component, OnInit} from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
// import { FormGroup, FormBuilder, Validators } from '@angular/forms'
import { VerificationPage } from '../register/verification/verification';
import { FormControl, FormGroup, Validators,ValidatorFn,AbstractControl } from '@angular/forms';


@Component({
    selector: 'page-register',
    templateUrl: 'register.html',
})
export class RegisterPage implements OnInit  {
  
  user: FormGroup;
    constructor(public navCtrl: NavController, public navParams: NavParams) {
     
    }

    ngOnInit() {

    this.user = new FormGroup({
    Username: new FormControl('', [Validators.required, Validators.minLength(4)]),
    // email: new FormControl('', [Validators.required,Validators.email]),
    email: new FormControl('', [Validators.required,Validators.pattern('^[A-Z0-9a-z\._%±]+@([A-Za-z0-9-]+\.)+[A-Za-z]{2,4}$')]),
    mob: new FormControl('', [Validators.required, Validators.minLength(10),Validators.maxLength(10)]),
    password: new FormControl('', [Validators.required, , Validators.minLength(6)]),
    re_password: new FormControl('', [Validators.required,this.equalto('password')])
    });

    }
    
    equalto(field_name): ValidatorFn {
    return (control: AbstractControl): {[key: string]: any} => {

    let input = control.value;

    let isValid=control.root.value[field_name]==input
    if(!isValid) 
    return { 'equalTo': {isValid} }
    else 
    return null;
    };
    }

    ionViewDidLoad() {
      console.log('ionViewDidLoad RegisterPage');
    }

    goToverification(){
      this.navCtrl.push(VerificationPage);
  } 
}

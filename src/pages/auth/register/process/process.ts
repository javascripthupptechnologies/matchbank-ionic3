import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

import { ProcessCompletePage } from '../process-complete/process-complete';




@Component({
  selector: 'page-process',
  templateUrl: 'process.html',
})
export class ProcessPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ProcessPage');
  }

  public workoutProgress: string = '25' + '%';
 
	updateProgress(val) {
	 // Update percentage value where the above is a decimal
	  this.workoutProgress = Math.min( (val * 100), 100) + '%';
	}

  goTofail(){
      this.navCtrl.push(ProcessCompletePage);
  } 

}

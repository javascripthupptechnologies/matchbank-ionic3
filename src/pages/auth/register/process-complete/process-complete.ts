import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

// import { MyAccountPage } from '../../../my-account/my-account';
import { LoginPage } from '../../../auth/login/login';
import { SplashPage } from '../../../auth/splash/splash';

@Component({
  selector: 'page-process-complete',
  templateUrl: 'process-complete.html',
})
export class ProcessCompletePage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ProcessCompletePage');
  }

  
  goTosplash(){
      this.navCtrl.push(SplashPage);
  } 
}

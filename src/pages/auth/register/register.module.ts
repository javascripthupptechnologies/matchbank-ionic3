import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';

import { RegisterPage } from './register';
import { VerificationPage } from '../register/verification/verification';
import { UploadPage } from '../register/upload/upload';
import { TermsPage } from '../register/terms/terms';
import { SuccessPage } from '../register/success/success';
import { ProcessCompletePage } from '../register/process-complete/process-complete';
import { ProcessPage } from '../register/process/process';
import { FailPage } from '../register/fail/fail';

@NgModule({
  declarations: [
    RegisterPage,
    VerificationPage,
    UploadPage,
    TermsPage,
    SuccessPage,
    ProcessCompletePage,
    ProcessPage,
    FailPage
  ],
  imports: [
    IonicPageModule.forChild(RegisterPage),
  ],
  entryComponents:[
  	VerificationPage,
    UploadPage,
    TermsPage,
    SuccessPage,
    ProcessCompletePage,
    ProcessPage,
    FailPage
  ]
})
export class RegisterPageModule {}

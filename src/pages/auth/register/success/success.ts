import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

import { TermsPage } from '../terms/terms';



@Component({
  selector: 'page-success',
  templateUrl: 'success.html',
})
export class SuccessPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SuccessPage');
  }
  
  goToterms(){
	    this.navCtrl.setRoot(TermsPage);
	} 

}

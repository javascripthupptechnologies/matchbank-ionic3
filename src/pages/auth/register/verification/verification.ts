import { Component, OnInit } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

import { SuccessPage } from '../success/success';
import { RegisterPage } from '../register';
import { FormControl, FormGroup, Validators,ValidatorFn,AbstractControl } from '@angular/forms';



@Component({
  selector: 'page-verification',
  templateUrl: 'verification.html',
})
export class VerificationPage implements OnInit {
  user: FormGroup;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ngOnInit() {

    this.user = new FormGroup({
    mob: new FormControl('', [Validators.required, Validators.minLength(4),Validators.maxLength(4)])
    });

    }

  ionViewDidLoad() {
    console.log('ionViewDidLoad VerificationPage');
  }

  goTosuccess(){
	    this.navCtrl.push(SuccessPage);
	} 
  goToregister(){
      this.navCtrl.push(RegisterPage);
  } 
}

import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

import { UploadPage } from '../upload/upload';



@Component({
  selector: 'page-terms',
  templateUrl: 'terms.html',
})
export class TermsPage {
	public pet:any;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
  		this.pet = "Step1";
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad TermsPage');
  }

 goToupload(){
	    this.navCtrl.setRoot(UploadPage);
	} 


}

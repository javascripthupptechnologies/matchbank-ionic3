import { Component, OnInit } from '@angular/core';
import { NavController, NavParams, LoadingController, ActionSheetController } from 'ionic-angular';
import { FormControl, FormGroup, Validators,ValidatorFn,AbstractControl } from '@angular/forms';

import { ProcessPage } from '../process/process';
import { Camera, CameraOptions } from '@ionic-native/camera';



@Component({
  selector: 'page-upload',
  templateUrl: 'upload.html',
})
export class UploadPage implements OnInit {
	public upload:any;
  user: FormGroup;
  public active:boolean = false;
  public selected:any = "SELECT ANSWER";
  constructor(public navCtrl: NavController, public navParams: NavParams, public loadingCtrl: LoadingController, private camera: Camera, private actionSheetCtrl:ActionSheetController) {
  	this.upload = "Step1";
  }

  ngOnInit() {

  this.user = new FormGroup({
  mob: new FormControl('', [Validators.required, Validators.minLength(6),Validators.maxLength(12)])
  });

  function myCtrl($scope) {
        $scope.uploadImage = function () {
            console.log("Changed");
        }
    }
  
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad UploadPage');
  }

   goToprocess(){
	    this.navCtrl.push(ProcessPage);
	} 

  onclickButton($event){
  console.log("click",$event);
  console.log("selected",this.selected);
  this.active = true;
  }

  public MykadBackImage:any;
  public MykadFrontImage:any;
  public takePicture(sourceType,flag) {
    let loading = this.loadingCtrl.create({
      content: 'Uploading...',
      spinner:'ios'
    });
    var options = {
      quality: 100,
      sourceType: sourceType,
      correctOrientation: true,
      destinationType: this.camera.DestinationType.DATA_URL,
      saveToPhotoAlbum: false,
      
    };
    this.camera.getPicture(options).then((imagePath) => {
      console.log("flag",flag);
      if(flag == true){
        this.MykadFrontImage = 'data:image/png;base64,' + imagePath;
      }else{
        this.MykadBackImage = 'data:image/png;base64,' + imagePath;
      }
      
      loading.dismiss();
      // this.imageUpload(base64Image);
    }, (err) => {
      loading.dismiss();
        // this.globalProvider.showToast('Error while selecting image.');
    });
  }

  public presentActionSheet(flag) {
    let actionSheet = this.actionSheetCtrl.create({
      title: 'Select Image Source',
      buttons: [
        {
        text: 'Load from Library',
          handler: () => {
            this.takePicture(this.camera.PictureSourceType.PHOTOLIBRARY,flag);
          }
        },
        {
        text: 'Use Camera',
          handler: () => {
            this.takePicture(this.camera.PictureSourceType.CAMERA,flag);
          }
        },
        {
        text: 'Cancel',
        role: 'cancel'
        }
      ]
      });
      actionSheet.present();
    }
}

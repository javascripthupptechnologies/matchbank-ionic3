import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AuthPage } from './auth';

import { RegisterPageModule } from '../auth/register/register.module';
import { LoginPage } from '../auth/login/login';
import { SplashPage } from '../auth/splash/splash';

@NgModule({
  declarations: [
    AuthPage,
    LoginPage,
    SplashPage
  ],
  imports: [
    IonicPageModule.forChild(AuthPage),
    RegisterPageModule
  ],
  entryComponents:[
  	LoginPage,
    SplashPage
  ]
})
export class AuthPageModule {}

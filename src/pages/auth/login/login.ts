import { Component, OnInit } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { TabsPage } from '../../tabs/tabs';
import { RegisterPage } from '../register/register';
import { FormControl, FormGroup, Validators,ValidatorFn,AbstractControl } from '@angular/forms';
/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage implements OnInit{
  user: FormGroup;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ngOnInit() {

  this.user = new FormGroup({
  // email: new FormControl('', [Validators.required,Validators.email]),
  email: new FormControl('', [Validators.required,Validators.pattern('^[A-Z0-9a-z\._%±]+@([A-Za-z0-9-]+\.)+[A-Za-z]{2,4}$')]),
  password: new FormControl('', [Validators.required, , Validators.minLength(6)]),
  });

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
  }

  goToreport(){
      this.navCtrl.push(TabsPage);
  }
  goToregister(){
      this.navCtrl.push(RegisterPage);
  } 
}

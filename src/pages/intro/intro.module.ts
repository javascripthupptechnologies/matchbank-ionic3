import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';

import { IntroPage } from './intro';
import { Introscreen2Page } from '../intro/introscreen2/introscreen2';
import { Introscreen3Page } from '../intro/introscreen3/introscreen3';

@NgModule({
  declarations: [
    IntroPage,
    Introscreen2Page,
    Introscreen3Page
  ],
  imports: [
    IonicPageModule.forChild(IntroPage),
  ],
  entryComponents:[
  	Introscreen2Page,
  	Introscreen3Page
  ]
})
export class IntroPageModule {}

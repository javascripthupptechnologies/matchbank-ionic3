import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { Introscreen2Page } from '../intro/introscreen2/introscreen2';

@IonicPage()
@Component({
  selector: 'page-intro',
  templateUrl: 'intro.html',
})
export class IntroPage {

	constructor(public navCtrl: NavController, public navParams: NavParams) {
	}

	ionViewDidLoad() {
	    console.log('ionViewDidLoad IntroPage');
	}

  	Intro2(){
 		this.navCtrl.push(Introscreen2Page);
  	}
}

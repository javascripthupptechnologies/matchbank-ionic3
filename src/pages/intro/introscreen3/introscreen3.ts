import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

import { RegisterPage } from '../../auth/register/register'


@Component({
  selector: 'page-introscreen3',
  templateUrl: 'introscreen3.html',
})
export class Introscreen3Page {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad Introscreen3Page');
  }


  	registerPage(){
			this.navCtrl.push(RegisterPage);
	}

}

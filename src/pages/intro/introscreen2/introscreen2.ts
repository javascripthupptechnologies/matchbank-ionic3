import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

import { Introscreen3Page } from '../introscreen3/introscreen3';

@Component({
  selector: 'page-introscreen2',
  templateUrl: 'introscreen2.html',
})
export class Introscreen2Page {

	constructor(public navCtrl: NavController, public navParams: NavParams) {
	}

	ionViewDidLoad() {
	    console.log('ionViewDidLoad Introscreen2Page');
	}

  	Intro3(){
		this.navCtrl.push(Introscreen3Page);
	}

}

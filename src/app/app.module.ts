import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';


import { TabsPage } from '../pages/tabs/tabs';


import { IntroPageModule }  from '../pages/intro/intro.module';
import { AuthPageModule } from '../pages/auth/auth.module';
import { MyAccountPageModule } from '../pages/my-account/my-account.module';



import { MyReportPageModule } from '../pages/my-report/my-report.module';
import { AnnouncePageModule } from '../pages/announce/announce.module';
import { MatchPageModule } from '../pages/match/match.module'
// import { NumberFormatterDirective } from "../directives/number-formatter/number-formatter";
import { Camera } from '@ionic-native/camera';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';


@NgModule({
  declarations: [
    MyApp,
    TabsPage
  ],
  imports: [
    BrowserModule,    
    IonicModule.forRoot(MyApp),
    MyReportPageModule,
    AnnouncePageModule,
    MatchPageModule,
    IntroPageModule,
    AuthPageModule,
    MyAccountPageModule,
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    TabsPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    Camera
  ]
})
export class AppModule {}
